const Cap = C => {return {
  r: f => 0,
  i: f => (-1 / (2 * Math.PI * f * C))
}};

const Ind = L => {return {
  r: f => 0,
  i: f => (2 * Math.PI * f * L)
}};

const Res = R => {return {
  r: f => R,
  i: f => 0
}};

const complex = (r, i) => {return {
  r: f => r, 
  i: f => i
}};

const cSum = (a, b) => {return {
  r: f => (a.r(f) + b.r(f)),
  i: f => (a.i(f) + b.i(f))
}};

const cDiv = (a, b) => {return {
  r: f => ((a.r(f) * b.r(f) + a.i(f) * b.i(f)) / (b.r(f) * b.r(f) + b.i(f) * b.i(f))),
  i: f => ((a.i(f) * b.r(f) - a.r(f) * b.i(f)) / (b.r(f) * b.r(f) + b.i(f) * b.i(f)))
}};

const sumX = key => a => f => R.sum(a.map(e => e[key](f)));
const sumReal = sumX('r');
const sumImag = sumX('i');

const serial = (...z) => {return {
  r: sumReal(z),
  i: sumImag(z)
}}

const parallel = (...z) => {
  const revs = R.map(b => cDiv(complex(1, 0), b), z)
  const rev = {
    r: sumReal(revs),
    i: sumImag(revs)
  };
  
  return cDiv(
    complex(1, 0),
    rev
  )
}

const mag = c => f => Math.sqrt(Math.pow(c.r(f), 2) + Math.pow(c.i(f), 2));

const val = (c, f) => {return{
  r: c.r(f),
  i: c.i(f)
}}

const a = parallel(Cap(880e-12), Res(220));

mag(a)(1000)